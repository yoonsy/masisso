/**
 * @jsx React.DOM
 */

var RestaurantBox = React.createClass({
  getInitialState: function() {
    return {
      regions: [],
      restaurants: []
    };
  },

  loadRestaurantsByRegion: function(region) {
    var restaurantQuery = new Parse.Query(ParseRestaurant);
    restaurantQuery.equalTo("region", region);
    restaurantQuery.find({
      success: function(restaurantsResults){
        var restaurants = [];
        for (var i = restaurantsResults.length - 1; i >= 0; i--) {
          restaurants.push(restaurantsResults[i].attributes);
        }
        this.setState({ restaurants: restaurants });
      }.bind(this)
    })
  },

  loadRegionsFromServer: function() {
    var regionQuery = new Parse.Query(ParseRegion);

    regionQuery.limit(1000);
    regionQuery.find({
      success: function(regionResults) {
        var regions = [];
        for (var i = 0, len = regionResults.length; i < len; i++) {

          if( i==0 ){
            this.loadRestaurantsByRegion(new ParseRegion({ id: regionResults[i].id }));
          }

          regions.push({
            id: regionResults[i].id,
            name: regionResults[i].attributes.name
          });

        };
        this.setState({ regions: regions });
      }.bind(this),
      error: function(error) {
        alert("Error: " + error.code + " " + error.message);
      }
    });
  },

  componentDidMount: function() {
    this.loadRegionsFromServer();
  },

  handleChangeRegion: function(region){
    this.loadRestaurantsByRegion(region);
  },

  handleRestaurantSubmit: function(restaurant){
    var restaurants = this.state.data;
    var newRestaurants = [restaurant].concat(restaurants);
    this.setState({data: newRestaurants});

    var parseRestaurant = new ParseRestaurant();

    parseRestaurant.set("region", new ParseRegion({ id: $('select').val() }));
    parseRestaurant.set("name", restaurant.name);
    parseRestaurant.set("desc", restaurant.desc);

    parseRestaurant.save(null, {
      success: function(restaurant) {
        this.loadRestaurantsByRegion(new ParseRegion({ id: restaurant.attributes.region.id }));
      }.bind(this),
      error: function(restaurant, error) {
        // Execute any logic that should take place if the save fails.
        // error is a Parse.Error with an error code and message.
        alert('Failed to create new object, with error code: ' + error.message);
      }
    })

  },

  render: function() {
    return (
      <div className="restaurantBox">
        <Region data={this.state.regions} onChangeRegion={this.handleChangeRegion} />
        <RestaurantForm onRestaurantSubmit={this.handleRestaurantSubmit} />
        <RestaurantList data={this.state.restaurants} />
      </div>
    );
  }
});