/**
 * @jsx React.DOM
 */

var RestaurantList = React.createClass({
  render: function() {
    var restaurantNodes = this.props.data.map(function (restaurant) {
      return (
        <Restaurant name={restaurant.name}>
          {restaurant.desc}
        </Restaurant>
      );
    });
    return (
      <div className="restaurantList">
        {restaurantNodes}
      </div>
    );
  }
});