/**
 * @jsx React.DOM
 */

var RestaurantForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var name = this.refs.name.getDOMNode().value.trim();
    var desc = this.refs.desc.getDOMNode().value.trim();
    if (!desc || !name) {
      return;
    }
    this.props.onRestaurantSubmit({name: name, desc: desc})
    this.refs.name.getDOMNode().value = '';
    this.refs.desc.getDOMNode().value = '';
    return;
  },
  render: function() {
    return (
      <form className="restaurantForm" onSubmit={this.handleSubmit}>
        <input type="text" placeholder="식당 이름" ref="name"/>
        <input type="text" placeholder="설명" ref="desc"/>
        <input type="submit" value="Post" />
      </form>
    );
  }
});