/**
 * @jsx React.DOM
 */

var Restaurant = React.createClass({
  render: function() {
    return (
      <div className="restaurant">
        <h2 className="restaurantName">
          {this.props.name}
        </h2>
        {this.props.children}
      </div>
    );
  }
});