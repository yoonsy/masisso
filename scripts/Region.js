/**
 * @jsx React.DOM
 */

var Region = React.createClass({
  handleChange: function(event) {

    this.props.onChangeRegion(new ParseRegion({ id: event.target.value }));

  },

  render: function() {
    var regionNodes = this.props.data.map(function (region) {
      return (
        <option value={region.id}>{region.name}</option>
      );
    });
    return (
      <h1>
        <select id="region-select" name="region" onChange={this.handleChange}>
        {regionNodes}
        </select>
        <span class="text">맛집</span>
      </h1>
    );
  }
});